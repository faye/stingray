use stingray::Beam;

fn main() -> stingray::Result<()> {
    let file = std::env::args().nth(1).expect("missing <file> argument");
    let data = std::fs::read(file)?;
    let beam = Beam::decode(&data)?;

    section("atoms");
    println!("{}", beam.atoms.join(", "));

    section("exports");
    for export in beam.exports.iter() {
        let function = &beam.atoms[export.function as usize - 1];
        print!("{}/{} [{}], ", function, export.arity, export.label);
    }
    println!();

    section("imports");
    for import in beam.imports.iter() {
        let module = &beam.atoms[import.module as usize - 1];
        let function = &beam.atoms[import.function as usize - 1];
        print!("{}:{}/{}, ", module, function, import.arity);
    }
    println!();

    section("strings");
    println!("{}", beam.strings);

    section("code");
    println!();
    for instr in beam.code.iter() {
        println!(" \x1b[90m|  \x1b[m{instr:?}");
    }

    Ok(())
}

fn section(name: &str) {
    print!("\x1b[90m[\x1b[92m+\x1b[90m] \x1b[96m{name:10}\x1b[m");
}
