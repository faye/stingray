// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

use super::code::Arg;
use crate::Error;

macro_rules! impl_opcode {
    ($($v:literal: $name:ident/$arity:literal)*) => {
        #[repr(u8)]
        #[derive(Debug, Clone)]
        pub enum Instr {
            $($name([Arg; $arity]) = $v),*
        }

        impl Instr {
            pub fn arity(byte: u8) -> Result<usize, Error> {
                match byte {
                    $($v => Ok($arity),)*
                    _ => Err(Error::Invalid),
                }
            }

            pub fn from_parts(byte: u8, args: Vec<Arg>) -> Result<Self, Error> {
                match byte {
                    $($v => Ok(Self::$name(args.try_into().unwrap())),)*
                    _ => Err(Error::Invalid),
                }
            }

            pub fn value(&self) -> u8 {
                match self {
                    $(Self::$name(_) => $v,)*
                }
            }

            pub fn args(&self) -> &[Arg] {
                match self {
                    $(Self::$name(a) => a,)*
                }
            }
        }
    };
}

// TODO: something better than `:read !sed -E '/^$|^#|-/d; s/(\b|_)([a-z])/\U\2/g' genop.tab`
//
// [!] The following code is generated from erlang/otp/lib/compiler/src/genop.tab
impl_opcode! {
    1: Label/1
    2: FuncInfo/3
    3: IntCodeEnd/0
    4: Call/2
    5: CallLast/3
    6: CallOnly/2
    7: CallExt/2
    8: CallExtLast/3
    9: Bif0/2
    10: Bif1/4
    11: Bif2/5
    12: Allocate/2
    13: AllocateHeap/3
    16: TestHeap/2
    18: Deallocate/1
    19: Return/0
    20: Send/0
    21: RemoveMessage/0
    22: Timeout/0
    23: LoopRec/2
    24: LoopRecEnd/1
    25: Wait/1
    26: WaitTimeout/2
    39: IsLt/3
    40: IsGe/3
    41: IsEq/3
    42: IsNe/3
    43: IsEqExact/3
    44: IsNeExact/3
    45: IsInteger/2
    46: IsFloat/2
    47: IsNumber/2
    48: IsAtom/2
    49: IsPid/2
    50: IsReference/2
    51: IsPort/2
    52: IsNil/2
    53: IsBinary/2
    55: IsList/2
    56: IsNonemptyList/2
    57: IsTuple/2
    58: TestArity/3
    59: SelectVal/3
    60: SelectTupleArity/3
    61: Jump/1
    62: Catch/2
    63: CatchEnd/1
    64: Move/2
    65: GetList/3
    66: GetTupleElement/3
    67: SetTupleElement/3
    69: PutList/3
    72: Badmatch/1
    73: IfEnd/0
    74: CaseEnd/1
    75: CallFun/1
    77: IsFunction/2
    78: CallExtOnly/2
    89: BsPutInteger/5
    90: BsPutBinary/5
    91: BsPutFloat/5
    92: BsPutString/2
    96: Fmove/2
    97: Fconv/2
    98: Fadd/4
    99: Fsub/4
    100: Fmul/4
    101: Fdiv/4
    102: Fnegate/3
    104: Try/2
    105: TryEnd/1
    106: TryCase/1
    107: TryCaseEnd/1
    108: Raise/2
    109: BsInit2/6
    111: BsAdd/5
    112: Apply/1
    113: ApplyLast/2
    114: IsBoolean/2
    115: IsFunction2/3
    117: BsGetInteger2/7
    118: BsGetFloat2/7
    119: BsGetBinary2/7
    120: BsSkipBits2/5
    121: BsTestTail2/3
    124: GcBif1/5
    125: GcBif2/6
    129: IsBitstr/2
    131: BsTestUnit/3
    132: BsMatchString/4
    133: BsInitWritable/0
    134: BsAppend/8
    135: BsPrivateAppend/6
    136: Trim/2
    137: BsInitBits/6
    138: BsGetUtf8/5
    139: BsSkipUtf8/4
    140: BsGetUtf16/5
    141: BsSkipUtf16/4
    142: BsGetUtf32/5
    143: BsSkipUtf32/4
    144: BsUtf8Size/3
    145: BsPutUtf8/3
    146: BsUtf16Size/3
    147: BsPutUtf16/3
    148: BsPutUtf32/3
    149: OnLoad/0
    152: GcBif3/7
    153: Line/1
    154: PutMapAssoc/5
    155: PutMapExact/5
    156: IsMap/2
    157: HasMapFields/3
    158: GetMapElements/3
    159: IsTaggedTuple/4
    160: BuildStacktrace/0
    161: RawRaise/0
    162: GetHd/2
    163: GetTl/2
    164: PutTuple2/2
    165: BsGetTail/3
    166: BsStartMatch3/4
    167: BsGetPosition/3
    168: BsSetPosition/2
    169: Swap/2
    170: BsStartMatch4/4
    171: MakeFun3/3
    172: InitYregs/1
    173: RecvMarkerBind/2
    174: RecvMarkerClear/1
    175: RecvMarkerReserve/1
    176: RecvMarkerUse/1
    177: BsCreateBin/6
    178: CallFun2/3
    179: NifStart/0
    180: Badrecord/1
    181: UpdateRecord/5
    182: BsMatch/3
    183: ExecutableLine/1
}
