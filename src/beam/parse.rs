use super::{
    code::{Arg, Instr, Tag},
    Atom, Beam, Builder, Export, Import, BEAM_FORMAT, MAX_OPCODE,
};
use crate::{etf::Term, Error};

pub struct Parser<'a> {
    data: &'a [u8],
}

impl<'a> Parser<'a> {
    pub fn new(data: &'a [u8]) -> Self {
        Self { data }
    }

    pub fn parse(&mut self) -> Result<Beam, Error> {
        let mut builder = Builder::default();

        // beam files start with `FOR1 [length] BEAM`
        self.parse_header()?;

        // loop through the file chunks
        while !self.data.is_empty() {
            // each chunk is prefixed by a four character name
            // a chunk is just the raw data, prefixed with it's length
            let name = self.read_n::<4>()?;
            let chunk = self.read_chunk()?;

            match &name {
                b"Atom" | b"AtU8" => {
                    builder = builder.atoms(Parser::new(chunk).parse_atoms()?);
                }
                b"Code" => {
                    builder = builder.code(Parser::new(chunk).parse_code()?);
                }
                b"StrT" => {
                    builder = builder.strings(std::str::from_utf8(chunk)?);
                }
                b"LitT" => {
                    let chunk = Parser::new(chunk).decompress()?;
                    println!("{:?}", Parser::new(&chunk).parse_literals()?);
                }
                b"ImpT" => {
                    builder = builder.imports(Parser::new(chunk).parse_imports()?);
                }
                b"ExpT" => {
                    builder = builder.exports(Parser::new(chunk).parse_exports()?);
                }
                _ => {}
            }
        }

        builder.build()
    }

    fn parse_header(&mut self) -> Result<(), Error> {
        match (&self.read_n()?, self.read_length()?, &self.read_n()?) {
            (b"FOR1", _length, b"BEAM") => Ok(()),
            _ => Err(Error::InvalidHeader),
        }
    }

    fn parse_atoms(&mut self) -> Result<Vec<Atom>, Error> {
        let mut atoms = Vec::with_capacity(self.read_length()? as usize);

        while !self.data.is_empty() {
            let size = u8::from_be_bytes(self.read_n()?) as usize;
            let atom = self.read(size)?;

            atoms.push(std::str::from_utf8(atom)?.into());
        }

        Ok(atoms)
    }

    fn parse_code(&mut self) -> Result<Vec<Instr>, Error> {
        let info_size = self.read_length()? as usize;
        let format = self.read_length()?;
        let max_opcode = self.read_length()?;

        if format != BEAM_FORMAT || max_opcode > MAX_OPCODE {
            return Err(Error::Incompatible);
        }

        self.read(info_size - 8)?; // trailing info

        let mut code = Vec::new();
        while !self.data.is_empty() {
            let b = self.read_byte()?;
            let args = std::iter::repeat_with(|| self.parse_arg())
                .take(Instr::arity(b)?)
                .collect::<Result<Vec<_>, _>>()?;

            code.push(Instr::from_parts(b, args)?);
        }

        Ok(code)
    }

    fn parse_arg(&mut self) -> Result<Arg, Error> {
        let b = self.read_byte()?;
        let tag = match b & 0b111 {
            t if t == Tag::None as u8 => Tag::None,
            t if t == Tag::Integer as u8 => Tag::Integer,
            t if t == Tag::Atom as u8 => Tag::Atom,
            t if t == Tag::RegX as u8 => Tag::RegX,
            t if t == Tag::RegY as u8 => Tag::RegY,
            t if t == Tag::Label as u8 => Tag::Label,
            t if t == Tag::Character as u8 => Tag::Character,
            _ => {
                let tag = match b {
                    t if t == Tag::List as u8 => Tag::List,
                    t if t == Tag::RegFp as u8 => Tag::RegFp,
                    t if t == Tag::AllocList as u8 => Tag::AllocList,
                    t if t == Tag::Literal as u8 => Tag::Literal,
                    _ => todo!("invalid tag"),
                };
                let v = match self.parse_arg()? {
                    Arg(Tag::None, n) => n,
                    _ => todo!("whar"),
                };
                return Ok(Arg(tag, v));
            }
        };

        let v = match tag {
            _ if b & 0x08 == 0 => (b >> 4) as u32,
            _ if b & 0b10 == 0 => ((b & 0xe0) << 3 | self.read_byte()?) as u32,
            _ => todo!(),
        };

        Ok(Arg(tag, v))
    }

    fn decompress(&mut self) -> Result<Vec<u8>, Error> {
        let size = self.read_length()? as usize;
        // miniz please name your funcs normally
        miniz_oxide::inflate::decompress_to_vec_zlib_with_limit(self.data, size)
            .map_err(|_| Error::Invalid)
    }

    fn parse_literals(&mut self) -> Result<Vec<Term>, Error> {
        let mut literals = Vec::with_capacity(self.read_length()? as usize);

        while !self.data.is_empty() {
            let size = self.read_length()? as usize;
            literals.push(Term::from_external(self.read(size)?)?);
        }

        Ok(literals)
    }

    fn parse_imports(&mut self) -> Result<Vec<Import>, Error> {
        let mut exports = Vec::with_capacity(self.read_length()? as usize);

        while !self.data.is_empty() {
            exports.push(Import {
                module: self.read_length()?,
                function: self.read_length()?,
                arity: self.read_length()?,
            })
        }

        Ok(exports)
    }

    fn parse_exports(&mut self) -> Result<Vec<Export>, Error> {
        let mut exports = Vec::with_capacity(self.read_length()? as usize);

        while !self.data.is_empty() {
            exports.push(Export {
                function: self.read_length()?,
                arity: self.read_length()?,
                label: self.read_length()?,
            })
        }

        Ok(exports)
    }

    fn read(&mut self, bytes: usize) -> Result<&[u8], Error> {
        if bytes > self.data.len() {
            return Err(Error::Eof);
        }

        let (res, data) = self.data.split_at(bytes);
        self.data = data;
        Ok(res)
    }

    fn read_n<const N: usize>(&mut self) -> Result<[u8; N], Error> {
        Ok(self.read(N)?.try_into().unwrap())
    }

    fn read_byte(&mut self) -> Result<u8, Error> {
        let (&b, data) = self.data.split_first().ok_or(Error::Eof)?;
        self.data = data;
        Ok(b)
    }

    /// Read a 32-bit number
    fn read_length(&mut self) -> Result<u32, Error> {
        Ok(u32::from_be_bytes(self.read_n()?))
    }

    /// Read a length-prefixed chunk
    fn read_chunk(&mut self) -> Result<&[u8], Error> {
        let size = self.read_length()? as usize;
        // chunks are padded with zeroes for memory alignment reasons
        let padding = (4 - size % 4) % 4;
        let chunk = self.read(size + padding)?;

        Ok(&chunk[..size])
    }
}
