// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

pub use super::opcode::Instr;

#[derive(Debug, Clone, Copy)]
pub struct Arg(pub Tag, pub u32);

#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Tag {
    None = 0b000,
    Integer = 0b001,
    Atom = 0b010,
    RegX = 0b011,
    RegY = 0b100,
    Label = 0b101,
    Character = 0b110,
    // extended tags
    List = 0b0001_0111,
    RegFp = 0b0010_0111,
    AllocList = 0b0011_0111,
    Literal = 0b0100_0111,
}
