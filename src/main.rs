// stingray, interfaces for Erlang and the BEAM
// Copyright (c) 2023 rini
//
// SPDX-License-Identifier: Apache-2.0

use std::process::Command;

use stingray::beam::{
    code::{Arg, Instr, Tag},
    AtomTable, Export, Import,
};

fn main() -> stingray::Result<()> {
    let mut atoms = AtomTable::new("nya");

    let run = atoms.add("run");
    let io = atoms.add("io");
    let put_chars = atoms.add("put_chars");

    let beam = stingray::Beam::builder()
        .atoms(atoms)
        .code([
            Instr::Label([Arg(Tag::None, 1)]),
            Instr::Move([Arg(Tag::Literal, 0), Arg(Tag::RegX, 0)]),
            Instr::CallExtOnly([Arg(Tag::None, 1), Arg(Tag::None, 0)]),
            Instr::IntCodeEnd([]),
        ])
        .exports([Export {
            function: run,
            label: 1,
            arity: 0,
        }])
        .imports([Import {
            module: io,
            function: put_chars,
            arity: 1,
        }])
        .build()?;

    std::fs::write("nya.beam", beam.encode())?;

    Command::new("erl")
        .args(["-noshell", "-eval", "nya:run(), erlang:halt(0)"])
        .spawn()?
        .wait()?;

    Ok(())
}
